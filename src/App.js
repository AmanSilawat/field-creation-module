// import logo from './logo.svg';
import React, { useState, useEffect } from 'react';
import './App.css';
import Table from './components/Table'
import ColumnCreation from './components/ColumnCreation';
import myData from './myData.js';

function App() {

  const initial_columns = [
    {
      Header: 'Name',
      accessor: 'name',
      type: 'text'
    },
    {
      Header: 'Age',
      accessor: 'age',
      type: 'number'
    },
    {
      Header: 'Date',
      accessor: 'date',
      type: 'date'
    },
  ];

  const [columns, setColumns] = useState(initial_columns)
  const [data, setData] = useState(() => myData());
  const [originalData] = useState(data);
  const [skipPageReset, setSkipPageReset] = React.useState(false);

  const updateMyData = (rowIndex, columnId, value) => {
    // We also turn on the flag to not reset the page
    setSkipPageReset(true)
    setData(old =>
      old.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          }
        }
        return row
      })
    )
  }

  const append_column = ({ column, value, type }) => {

    // if type is multiSelect
    const multiselect_list = type === 'multiselect' ? value : null;
    const cell_value = type === 'multiselect' ? value[0] : value;
    const accessor = column.toLowerCase();

    for (const item of columns) {
      if (item.accessor === accessor) {
        return 'duplicate_entry';
      }
    }


    // set column data
    setColumns(old => {
      return [
        ...old,
        {
          Header: column.replace(/^\w/, c => c.toUpperCase()),
          accessor,
          type,
          list: multiselect_list
        },
      ]
    })


    // set table data
    setData(old =>
      old.map((row, index) => {
        row[accessor] = cell_value;
        return row
      })
    )

    return 'success'
  }

  const resetData = () => setData(originalData)

  React.useEffect(() => {
    setSkipPageReset(false)
  }, [data])

  const submitHandler = () => {
    console.log('data', data)
    alert('show in console for show table data')
  }

  return (
    <div className="page">
      <div className="table_wrapper">
        <div className="container">
          <h1 className="main_heading">Table Creation Model</h1>
          <Table
            columns={columns}
            data={data}
            updateMyData={updateMyData}
            skipPageReset={skipPageReset}
          />
          <div className="btn_wrapper">
            <button className="btn_sty btn_inverse" onClick={submitHandler}>Submit</button>
            <button className="btn_sty" onClick={resetData}>Reset Data</button>
          </div>
        </div>
      </div>
      <ColumnCreation append_column={append_column}></ColumnCreation>
    </div>
  );
}

export default App;
