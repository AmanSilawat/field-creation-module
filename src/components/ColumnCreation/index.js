import React, { useState } from 'react';
import produce from "immer";
import Select from 'react-select';
import './style.css';

function ColumnCreation({ append_column }) {
    const [is_multiselect, set_is_multiselect] = useState(false);
    const [is_name_valid, set_is_name_valid] = useState(true);
    const [is_multiselect_valid, set_is_multiselect_valid] = useState(true);
    const [form_data, set_form_data] = useState({ name: '', type: 'number', value: '' });

    const type_handler = (event) => {
        switch (event.value) {
            case 'multiselect':
                set_is_multiselect(true)
                break;

            case 'number':
                set_form_data({
                    ...form_data,
                    type: 'number'
                })
                set_is_multiselect(false)
                break;

            case 'date':
                set_form_data({
                    ...form_data,
                    type: 'date'
                })
                set_is_multiselect(false)
                break;

            default:
                set_is_multiselect(false)
                break;

        }
    }

    const field_handler = (event) => {
        const field_value = event.target.value.trim();
        switch (event.target.name) {
            case 'name':
                if (field_value === '') {
                    set_is_name_valid(false);
                } else {
                    set_form_data({
                        ...form_data,
                        name: field_value,
                        value: 'number'
                    })
                    set_is_name_valid(true);
                }
                break;

            case 'multiselect':
                if (field_value === '') {
                    set_is_multiselect_valid(false);
                } else {
                    set_form_data({
                        ...form_data,
                        type: 'multiselect',
                        value: field_value
                    })
                    set_is_multiselect_valid(true);
                }

                break;

            default:
                break;
        }
    }

    const column_creation_handler = (event) => {
        event.preventDefault();

        if (form_data.name !== '' && form_data.type !== '') {

            let column_value = form_data.type === 'multiselect'
                ? form_data.value.split(',').map(el => el.trim())
                : form_data.type

            const append_status = append_column({
                column: form_data.name,
                value: column_value,
                type: form_data.type
            });

            switch (append_status) {
                case 'duplicate_entry':
                    alert('please enter duplicate name')
                    break;

                case 'success':
                    event.target.reset();
                    set_is_multiselect(false)
                    break;

                default:
                    break;
            }

        } else {
            set_is_name_valid(false)
            set_is_multiselect_valid(false)
        }
    }

    return (
        <div className="column_creation">
            <div className="container">
                <form className="form_wrapper" onSubmit={column_creation_handler}>
                    <h2 className="heading">Column Creation Module</h2>
                    <div className="field_grp">
                        <label className="label" htmlFor="name">Name</label>
                        <input className="box_field" id="name" name="name" onChange={field_handler} type="text" placeholder="Enter field name" />
                        {
                            is_name_valid === false
                                ? <div className="error">Please enter some text</div>
                                : null
                        }
                    </div>
                    <div className="field_grp">
                        <label className="label" htmlFor="type">Type</label>
                        <Select id="type" options={[
                            { value: 'number', label: 'Number' },
                            { value: 'date', label: 'Date' },
                            { value: 'multiselect', label: 'MultiSelect' }
                        ]}
                            onChange={type_handler}
                        />
                    </div>
                    {
                        is_multiselect === true
                            ? (
                                <div className="field_grp">
                                    <label className="label" htmlFor="multiselect">Multi select</label>
                                    <input className="box_field" id="multiselect" name="multiselect" onChange={field_handler} type="text" placeholder="String like: delhi, mumbai" />
                                    {
                                        is_multiselect_valid === false
                                            ? <div className="error">Please enter some text</div>
                                            : null
                                    }
                                </div>
                            )
                            : null
                    }
                    <div className="field_btn_grp">
                        <button className="btn_sty field">submit</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default ColumnCreation;
