import React, { useState } from 'react';
import './style.css'
import TableData from './TableData';
import produce from "immer";
import { useTable, usePagination } from 'react-table';

import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
// import Select from '../Select/Select';
import Select from 'react-select';


// Create an editable cell renderer
const EditableCell = ({ value: initialValue, row: { index }, column: { id, type, list = [] }, updateMyData, }) => {
    // We need to keep and update the state of the cell normally
    const [value, setValue] = React.useState(initialValue)

    const onChange = e => {
        setValue(e.target.value)
    }

    const onChangeSelected = e => {
        setValue(e.value)
    }

    const onBlurSelected = e => {
        updateMyData(index, id, value)
    }

    // We'll only update the external data when the input is blurred
    const onBlur = () => {
        updateMyData(index, id, value)
    }

    // If the initialValue is changed external, sync it up with our state
    React.useEffect(() => {
        setValue(initialValue)
    }, [initialValue])

    switch (type) {
        case 'text':
            return <input className="input_field" type="text" value={value} onChange={onChange} onBlur={onBlur} placeholder="Enter text" />;

        case 'number':
            return <input className="input_field" type="number" value={value} onChange={onChange} onBlur={onBlur} placeholder="Enter number" />;

        case 'date':
            return <input className="input_field" type="date" value={value} onChange={onChange} onBlur={onBlur} />;

        case 'multiselect':
            return (
                <div className="field">
                    {/* <select className="input_field" onChange={onChange} onBlur={onBlur}>
                        {list.map((item, i) => <option key={i}>{item}</option>)}
                    </select> */}
                    <Select
                        options={list.map((item) => ({ value: item, label: item }))}
                        onChange={onChangeSelected}
                        onBlur={onBlur}
                        styles={{
                            control: (provided, state) => ({
                                ...provided,
                                width: '200px'
                            }),
                        }}
                    />
                </div>
            )

        default:
            return null;
    }
}

// Set our editable cell renderer as the default Cell renderer
const defaultColumn = {
    Cell: EditableCell,
}

function Table({ columns, data, updateMyData, skipPageReset }) {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state: { pageIndex, pageSize },
    } = useTable(
        {
            columns,
            data,
            defaultColumn,
            // use the skipPageReset option to disable page resetting temporarily
            autoResetPage: !skipPageReset,
            // updateMyData isn't part of the API, but
            // anything we put into these options will
            // automatically be available on the instance.
            // That way we can call this function from our
            // cell renderer!
            updateMyData,
        },
        usePagination
    )

    // Render the UI for your table
    return (
        <>
            <table className="data_table" {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div className="pagination">
                <button className="btn_sty" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                    <FirstPageIcon />
                </button>{' '}
                <button className="btn_sty" onClick={() => previousPage()} disabled={!canPreviousPage}>
                    <ChevronLeftIcon />
                </button>{' '}
                <button className="btn_sty" onClick={() => nextPage()} disabled={!canNextPage}>
                    <ChevronRightIcon />
                </button>{' '}
                <button className="btn_sty" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                    <LastPageIcon />
                </button>{' '}
                <span>
                    Page{' '}
                    <strong>
                        {pageIndex + 1} of {pageOptions.length}
                    </strong>{' '}
                </span>
                <span>
                    | Go to page:{' '}
                    <input
                        className="box_field"
                        type="number"
                        defaultValue={pageIndex + 1}
                        onChange={e => {
                            const page = e.target.value ? Number(e.target.value) - 1 : 0
                            gotoPage(page)
                        }}
                        style={{ width: '100px' }}
                    />
                </span>{' '}
                <Select
                    options={[
                        { value: '10', label: '10' },
                        { value: '20', label: '20' },
                        { value: '30', label: '30' }
                    ]}
                    onChange={(e) => setPageSize(Number(e.value))}
                    styles={{
                        control: (provided, state) => ({
                            ...provided,
                            width: '200px'
                        }),
                    }}
                />
            </div>
        </>
    )
}

function find_parent_el({ path, find }) {
    for (const el of path) {
        if (el.matches(find)) {
            return el
        } else if (el.matches('body')) {
            return null
        }
    }
    return null
}

function focus_validation({ current_el }) {
    // remove active class
    const wrapper_el = document.querySelector('.data_table');

    wrapper_el.querySelectorAll('.active').forEach(el => {
        el.classList.remove('active');
    });

    // add active class
    current_el.classList.add('active');
}

function navigation_focus({ key_type, event }) {
    switch (key_type) {

        // left key
        case 37: {
            const td_el = find_parent_el({ path: event.nativeEvent.path, find: 'td' })
            const prevSibling = td_el.previousElementSibling;
            const firstChild = td_el.parentElement.firstChild;
            if (prevSibling != null && prevSibling !== firstChild) {

                prevSibling.querySelector('.input_field').focus();
                focus_validation({ current_el: prevSibling })
            }
            break;
        }

        // Up key
        case 38: {
            const td_el = find_parent_el({ path: event.nativeEvent.path, find: 'td' })

            const cellIndex = td_el.cellIndex;
            const prev_row = td_el.parentElement.previousElementSibling;

            if (prev_row != null) {
                const prev_td_el = prev_row.children[cellIndex];

                prev_td_el.querySelector('.input_field').focus();
                focus_validation({ current_el: prev_td_el })
            }
            break;
        }

        // right key
        case 39: {
            const td_el = find_parent_el({ path: event.nativeEvent.path, find: 'td' })
            const nextSibling = td_el.nextElementSibling;

            if (nextSibling != null) {

                nextSibling.querySelector('.input_field').focus();
                focus_validation({ current_el: nextSibling })
            }
            break;
        }

        // Down key
        case 40: {
            const td_el = find_parent_el({ path: event.nativeEvent.path, find: 'td' })

            const cellIndex = td_el.cellIndex;
            const next_row = td_el.parentElement.nextElementSibling;

            if (next_row != null) {
                const next_td_el = next_row.children[cellIndex];

                next_td_el.querySelector('.input_field').focus();
                focus_validation({ current_el: next_td_el })
            }
            break;
        }

        default:
            break;
    }
}
export default Table

