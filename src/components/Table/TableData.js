import React from 'react'

function TableData({ cell_payload, keyup_handler, click_handler, set_input_handler }) {
    switch (cell_payload.type) {
        case 'text':
            return <input className="input_field" onKeyUp={keyup_handler} placeholder="Enter data" onChange={set_input_handler} type="text"></input>;

        case 'date':
            return <input className="input_field" onKeyUp={keyup_handler} placeholder="Enter data" type="date"></input>;
        
        case 'number':
            return <input className="input_field" onKeyUp={keyup_handler} placeholder="Enter data" type="number"></input>;

        case 'multiselect':
            return (
                <div className="field">
                    <select className="input_field" onKeyUp={keyup_handler}>
                        {cell_payload.value.map((item, i) => <option key={i}>{item}</option>)}
                    </select>
                </div>
            )
        default:
            return null;
    }
}

export default TableData;
